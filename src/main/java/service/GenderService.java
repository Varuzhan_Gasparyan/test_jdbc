package service;

import dao.GenderDAO;
import models.Genger;

import java.sql.SQLException;
import java.util.List;

public class GenderService {

    private GenderDAO genderDAO;

    public void insert(Genger genger) throws SQLException {
        genderDAO.insert(genger);
    }
    public List<Genger> select() throws SQLException {
        return genderDAO.select();
    }
    public void update(int id, String value) throws SQLException {
        genderDAO.update(id,value);
    }


    public GenderService(GenderDAO genderDAO) {
        this.genderDAO = genderDAO;
    }
}
