package service;

import dao.UserDAO;
import models.User;

import java.sql.SQLException;
import java.util.List;

public class UserService {
    private UserDAO userDAO;

    public void insert(User user) throws SQLException {
        userDAO.insert(user);
    }

    public List<User> select() throws SQLException {
        return userDAO.select();
    }

    public List<User> searchByName(String name) throws SQLException {
        return userDAO.searchByName(name);
    }
    public List<User> searchByGender(int id)throws  SQLException{
       return userDAO.searchByGender(id);
    }
    public List<User> searchByAddress(String address) throws SQLException{
        return userDAO.searchByAddress(address);
    }

    public void update(String fname, String value) throws SQLException {
        userDAO.update(fname, value);
    }

    public void delete(String lname) throws SQLException {
        userDAO.delete(lname);
    }
    public List<User> search(String name) throws SQLException{
     return    userDAO.searchByName(name);
    }

    public UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
}
