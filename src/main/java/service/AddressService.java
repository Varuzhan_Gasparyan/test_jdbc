package service;

import dao.AddressDAO;
import models.Address;

import java.sql.SQLException;
import java.util.List;

public class AddressService {
    private AddressDAO addressDAO;

    public void insert(Address address) throws SQLException {
        addressDAO.insert(address);
    }

    public List<Address> select() throws SQLException {
        return addressDAO.select();
    }

    public void update(int id, String value) throws SQLException {
        addressDAO.update(id, value);
    }

    public void delete(int id) throws SQLException {
        addressDAO.delete(id);
    }


    public AddressService(AddressDAO addressDAO) {
        this.addressDAO = addressDAO;
    }
}
