package dao;

import models.Address;
import models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AddressDAO {
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbcuser?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
    }

    public void insert(Address address) throws SQLException {
        try (Statement stmt = getConnection().createStatement()) {
            stmt.execute("INSERT INTO address (ADDRESS ) VALUES (' " + address.getAddress()
                    + "')");
        }
    }

    public List<Address> select() throws SQLException {
        List<Address> addressList = new ArrayList();
        Address address = new Address();
        try (Statement stmt = getConnection().createStatement()) {
            ResultSet resultSet = stmt.executeQuery(" SELECT * FROM  address;");
            while (resultSet.next()) {
                address.setAddress(resultSet.getString("ADDRESS"));
                address.setId(resultSet.getInt("ID"));
                 addressList.add(address);
            }
        }
        return addressList;
    }

    public void update(int id, String value) throws SQLException {
        try (Statement stmt = getConnection().createStatement()) {
            int resultSet = stmt.executeUpdate(" UPDATE address SET address ='" + value + "' WHERE ADDRESS_ID = '" + id + "\'");
        }
    }

    public void delete(int id) throws SQLException {
        try (Statement stmt = getConnection().createStatement()) {
            int resultSet = stmt.executeUpdate("DELETE FROM address WHERE address_id =" + "\'" + id + "\'");
        }
    }


}
