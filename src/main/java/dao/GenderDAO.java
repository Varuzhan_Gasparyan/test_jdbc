package dao;

import models.Address;
import models.Genger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GenderDAO {
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbcuser?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
    }

    public void insert(Genger genger) throws SQLException {
        try (Statement stmt = getConnection().createStatement()) {
            stmt.execute("INSERT INTO gender (GENDER ) VALUES (' " + genger.getGenderValue()
                    + "')");
        }
    }

    public List<Genger> select() throws SQLException {
        List<Genger> gengerList = new ArrayList<>();
        Genger genger = new Genger();
        try (Statement stmt = getConnection().createStatement()) {
            ResultSet resultSet = stmt.executeQuery(" SELECT * FROM gender;");
            while (resultSet.next()) {
                genger.setGenderValue(resultSet.getString("GENDER"));
                genger.setId(resultSet.getInt("ID"));
                gengerList.add(genger);
            }
        }
        return gengerList;
    }

    public void update(int id, String value) throws SQLException {
        try (Statement stmt = getConnection().createStatement()) {
            int resultSet = stmt.executeUpdate(" UPDATE gender SET gender ='" + value + "' WHERE gender_id = '" + id + "\'");
        }
    }



}
