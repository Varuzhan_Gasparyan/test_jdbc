package dao;

import models.Address;
import models.Genger;
import models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbcuser?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
    }

    public void insert(User user) throws SQLException {

        try (Statement stmt = getConnection().createStatement()) {
            int userId = stmt.executeUpdate("INSERT INTO `USER` (lname, FNAME, GENDER  ) VALUES ('" + user.getLname() + "','" + user.getFname()
                    + "'," + user.getGender().getId() + " )", Statement.RETURN_GENERATED_KEYS);

            String query = "INSERT INTO user_address (USERS_ID, ADDRESS_ID ) VALUES ";
            for (Address address : user.getAddress()) {
                query += generateInsertValueAdress(userId, address.getId());
            }
            if (!user.getAddress().isEmpty()) {
                query = query.substring(0, query.length() - 1);
                stmt.execute(query);
            }
        }
    }

    private String generateInsertValueAdress(Integer userID, Integer addressid) {
        return "('" + userID + "','" + addressid + "'),";
    }

    private List<User> getUsers(String query) throws SQLException {

        List<User> userList = new ArrayList<>();
        User user = new User();
        try (Statement stmt = getConnection().createStatement()) {
            ResultSet resultSet = stmt.executeQuery(query);
            while (resultSet.next()) {
                if (user.getUsers_id() != resultSet.getInt("USER.ID")) {
                    userList.add(user);
                    if (user.getUsers_id() != 0) {
                        User user1 = new User();
                        user = user1;
                    }
                    user.setUsers_id(resultSet.getInt("id"));
                    user.setFname(resultSet.getString("FNAME"));
                    user.setLname(resultSet.getString("LNAME"));
                    user.setGender(new Genger(resultSet.getString("GENDER" + "." + "GENDER")));
                    user.getAddress().add(new Address(resultSet.getString("ADDRESS.ADDRESS"), resultSet.getInt("ADDRESS.ID")));

                } else {
                    user.getAddress().add(new Address(resultSet.getString("ADDRESS.ADDRESS"), resultSet.getInt("ADDRESS.ID")));
                }
            }
        }


        return userList;
    }


    public void update(String fname, String value) throws SQLException {
        try (Statement stmt = getConnection().createStatement()) {
            int resultSet = stmt.executeUpdate(" UPDATE `user` SET lname ='" + value + "' WHERE FNAME = '" + fname + "\'");
        }
    }

    public void delete(String lname) throws SQLException {
        try (Statement stmt = getConnection().createStatement()) {
            int resultSet = stmt.executeUpdate("DELETE FROM `user` WHERE lname =" + "\'" + lname + "\'");
        }
    }


    public List<User> select() throws SQLException {
        String query = " SELECT  * FROM  `user`  LEFT JOIN  gender ON user.`gender` \" +\n" +
                "                    \" = gender.`id` LEFT JOIN  user_address ON  user.id = user_address.`users_id` \\n\" +\n" +
                "                    \" LEFT JOIN address  ON  user_address.`address_id` = address.`id` ORDER BY `user`.`id`;";
        return getUsers(query);
    }


    public List<User> searchByName(String name) throws SQLException {
        String query = " SELECT  * FROM  `user`  LEFT JOIN  gender ON user.`gender` " +
                " = gender.`id` LEFT JOIN  user_address ON  user.id = user_address.`users_id` \n" +
                " LEFT JOIN address  ON  user_address.`address_id` = address.`id` " +
                " WHERE user.`lname` = '" + name + "' " + " ORDER BY `user`.`id`;";
        return getUsers(query);
    }

    public List<User> searchByGender(int id) throws SQLException {
        String query = " SELECT  * FROM  `user`  LEFT JOIN  gender ON user.`gender`  = gender.`id` LEFT JOIN  " +
                "user_address ON  user.id = user_address.`users_id` \n" +
                " LEFT JOIN address  ON  user_address.`address_id` = address.`id` " +
                " WHERE user.`gender` = " + id + " ORDER BY `user`.`id`;";
        return getUsers(query);
    }

    public List<User> searchByAddress(String address) throws SQLException {
        String query = "\n" +
                " SELECT  * FROM  `user`  LEFT JOIN  gender ON user.`gender`  = gender.`id`" +
                " LEFT JOIN  user_address ON  user.id = user_address.`users_id` \n" +
                " LEFT JOIN address  ON  user_address.`address_id` = address.`id` " +
                " WHERE address = " + address + "  ORDER BY `user`.`id`;";
        return getUsers(query);
    }
}