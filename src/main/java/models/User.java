package models;


import models.Address;

import java.util.ArrayList;
import java.util.List;

public class User {

    int users_id;
    String lname;
    String fname;
    Genger gender  = new Genger();
    List<Address> address = new ArrayList<>();


    public User( ) {
    }

    public User(String lname, String fname, Genger gender, List<Address> address) {

        this.lname = lname;
        this.fname = fname;
        this.gender = gender;
        this.address = address;
    }


    public User(String lname, String fname, Genger gender) {
        this.lname = lname;
        this.fname = fname;
        this.gender = gender;
    }

    public Integer getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Integer users_id) {
        this.users_id = users_id;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public Genger getGender() {
        return gender;
    }

    public void setGender(Genger gender) {
        this.gender = gender;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "users_id=" + users_id +
                ", lname='" + lname + '\'' +
                ", fname='" + fname + '\'' +
                ", gender=" + gender +
                ", address=" + address +
                '}';
    }
}

