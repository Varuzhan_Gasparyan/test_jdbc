package models;

public class Genger {
    int id;
    String genderValue;

    public Genger( ) {

    }

    public Genger(int id, String genderValue) {
        this.id = id;
        this.genderValue = genderValue;
    }

    public Genger(String genderValue) {

        this.genderValue = genderValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGenderValue() {
        return genderValue;
    }

    public void setGenderValue(String genderValue) {
        this.genderValue = genderValue;
    }

    @Override
    public String toString() {
        return "Genger{" +
                "id=" + id +
                ", genderValue='" + genderValue + '\'' +
                '}';
    }
}
